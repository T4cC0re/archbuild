#!/usr/bin/env bash

mkdir -p packages
chmod 777 packages &> /dev/null

MOUNTS=""

# While these will be passed through, we might need to set some flags
while getopts 'chd:a:s:t:' opt; do
  case ${opt} in
    c )
      # ccache does nothing without a build-cache
      MOUNTS="${MOUNTS} -v "$(pwd)/cache:/builds""
      ;;
    *)
      ;;
  esac
done

docker run --rm -e "DEBUG=${DEBUG}" -e "VERBOSE=${VERBOSE}" -u 65534:65534 ${MOUNTS} -v "$(pwd)/packages:/packages" registry.gitlab.com/t4cc0re/archbuild $@
