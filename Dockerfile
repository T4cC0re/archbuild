FROM archlinux:latest

RUN echo 'Server = https://mirror.leaseweb.net/archlinux/$repo/os/$arch' > /etc/pacman.d/mirrorlist
# Base installation
RUN pacman -Syu --noconfirm --noprogressbar base-devel ttf-roboto git curl ccache

# Add user, group sudo
RUN groupadd --system sudo && \
    usermod -aG  sudo nobody && \
    sed -i -e "s/Defaults    requiretty.*/ #Defaults    requiretty/g" /etc/sudoers && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

# Set correct locale
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment && \
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen && \
    echo "LANG=en_US.UTF-8" > /etc/locale.conf
RUN locale-gen en_US.UTF-8
ENV LC_CTYPE 'en_US.UTF-8'
RUN mkdir -p /builds /packages && chown -R nobody. /builds /packages && chmod -R 777 /builds /packages
ENV HOME=/builds
WORKDIR /builds
ADD wrapper.sh /usr/local/bin/wrapper
RUN chmod +x /usr/local/bin/wrapper
ENTRYPOINT ["/usr/local/bin/wrapper"]
USER nobody
