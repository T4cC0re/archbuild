#!/usr/bin/bash

if [ -n "${DEBUG}" ]; then
  set -x
  VERBOSE=1
fi

INDENT=0

showIndent () {
  local count=0
  while [ $count -lt $INDENT ]; do
    echo -n ">>"
    count=$(( $count + 1 ))
  done
  if [ $INDENT -ne 0 ]; then
    echo -n " "
  fi
}

increase() {
  INDENT=$(( $INDENT + 1 ))
}

decrease() {
  INDENT=$(( $INDENT - 1 ))
}

output () {
  local file="$1"
  if [ -z "${file}" ]; then
    file=/dev/null
  fi
  if [ -n "${VERBOSE}" ]; then
    tee "${file}" | sed -e 's/^/'"$(showIndent)"'/'
  else
    cat &> "${file}"
  fi
}

out () {
  echo "$@" | sed -e 's/^/'"$(showIndent)"'/' | sed 's/>>/##/g'
}

err () {
  out "${@}" 1>&2
}

sudo chown 65534:65534 /builds &>/dev/null
sudo chmod 777 /builds &>/dev/null
sudo rm -rf /builds/.gnupg &>/dev/null

THREADS=$(grep 'proc' /proc/cpuinfo | wc -l)
FORCED_PKGS=""
AUR_BASE="https://aur.archlinux.org"
CCACHE=0
BUILT_PACKAGES=""

while getopts "chd:a:s:t:" opt; do
  case ${opt} in
    h )
      echo 'Usage: [-h] [-d <forced dependency>] [-a <AUR base>] [-c] [-s] [-t] [--] <AUR Package ...>'
      echo
      echo ' -h show this help'
      echo ' -d Force a dependency to be installed before building packages. This may be used multiple times. The package must be in arch repos'
      echo ' -a Specify a different AUR base. Default '"'${AUR_BASE}'"
      echo ' -c Enable CCache'
      echo ' -s (Re-)set CCache size'
      echo ' -t Set make threads. Default: '"${THREADS}"
      exit 0
      ;;
    d )
      err "Forcing dependency: ${OPTARG}"
      FORCED_PKGS="${FORCED_PKGS} ${OPTARG}"
      ;;
    a )
      err "AUR base: ${OPTARG}"
      AUR_BASE="${OPTARG}"
      ;;
    c )
      err "CCache enabled"
      CCACHE=1
      ;;
    s )
      err "CCache size set to ${OPTARG}"
      CCACHE_SIZE="${OPTARG}"
      CCACHE=1
      ;;
    t )
      THREADS=${OPTARG}
      ;;
    * )
      echo "arg: ${OPTARG}"
      ;;
  esac
done
shift $((OPTIND-1))

echo 'CFLAGS="-march=native -mtune=native -O3 -pipe -fno-plt"' | sudo tee -a /etc/makepkg.conf >/dev/null
echo 'CXXFLAGS="-march=native -mtune=native -O3 -pipe -fno-plt"' | sudo tee -a /etc/makepkg.conf >/dev/null
echo 'COMPRESSXZ=(xz -c -z - --threads=0)' | sudo tee -a /etc/makepkg.conf >/dev/null
echo 'MAKEFLAGS="$MAKEFLAGS -j'"${THREADS}"'"' | sudo tee -a /etc/makepkg.conf >/dev/null
echo 'PKGEXT=".pkg.tar.xz"' | sudo tee -a /etc/makepkg.conf >/dev/null

if [[ $CCACHE -eq 1 ]]; then
  echo 'BUILDENV=(!distcc color ccache check !sign)' | sudo tee -a /etc/makepkg.conf >/dev/null
  mkdir -p /builds/.ccache
  if [[ -n "${CCACHE_SIZE}" ]]; then
    echo "max_size = ${CCACHE_SIZE}" > /builds/.ccache/ccache.conf
  fi
  ccache -s
fi

ERRORS=0
# Upstream make dependencies
UPSTREAM=""
# Aur packages to build incl. aur depencencies
AUR=""
MISSING=""
BUILT=""
# GPG keys required for building
GPG=""

out "Updating packages before building..."
increase
sudo pacman -Syu --noconfirm 2>&1 | output
decrease

declare -A makedeps
declare -A deppkgs

isAUR () {
  local check="$1"
  if pacman -Ss "^${check}\$" >/dev/null; then
    return 1
  else
    return 0
  fi
}

collectdeps () {
  local PKG="$1"
  shift
  local PARENTS="$@"

  if ! isAUR "${PKG}"; then
    out "Package ${PKG} is not an AUR package. Skipping..."
    decrease
    return
  fi

  if [ -z "${PARENTS}" ]; then
    AUR="${AUR} ${PKG}"
    out "Checking dependencies for ${PKG}..."
  else
    local prefix
    for PARENT in ${PARENTS}; do
      prefix="${prefix}${PARENT}->"
    done
    out "Checking sub-dependencies for ${prefix}${PKG}..."
  fi

  increase

  out "Grabbing PKGBUILD for ${PKG}..."
  increase
  if [ -d "/builds/${PKG}/.git" ]; then
    git -C "/builds/${PKG}" reset --hard 2>&1 | output
    git -C "/builds/${PKG}" checkout master 2>&1 | output
    git -C "/builds/${PKG}" pull 2>&1 | output
  else
    git clone "${AUR_BASE}/${PKG}.git" "/builds/${PKG}" 2>&1 | output
  fi
  decrease


  local DEPS="$(cat /builds/${PKG}/.SRCINFO | awk '$1~/depends/ {print $3}' | sed 's/[:><=]\(.\+\|\)//g' | xargs)"
  PKG_GPG="$(cat /builds/${PKG}/.SRCINFO | awk '$1~/^validpgpkeys/ {print $3}' | xargs)"

  if [ -n "${PKG_GPG}" ]; then
    GPG="${GPG} ${PKG_GPG}"
  fi

  out "Dependencies: ${DEPS}"

  increase
  for dep in ${DEPS}; do
    if isAUR "${dep}"; then
      makedeps[$PKG]="${makedeps[$PKG]} ${dep}"
      collectdeps "${dep}" "${PKG}" ${PARENTS}
    else
      UPSTREAM="${UPSTREAM} ${dep}"
    fi
  done
  decrease

  decrease
}

installmakedeps () {
  local PKG="$1"
  shift
  local PARENTS="$@"
  local count=0

  out "Building AUR dependencies for '${PKG}': ${makedeps[$PKG]}"

  for PARENT in ${PARENTS}; do
    if [ "${PARENT}" = "${PKG}" ]; then
      increase
        err "Loop detected. ${PKG} => ${PARENTS}"
      decrease
      return
    fi
  done

  local DEP_PKGS=""

  increase
  for dep in ${makedeps[$PKG]}; do
    count=$(( $count + 1 ))
    installmakedeps "${dep}" "${PKG}" ${PARENTS}
    buildpkg "${dep}" "${PKG}"
    DEP_PKGS="${DEP_PKGS} ${RET_PKGS}"
  done
  decrease

  increase
  if [ $count -eq 0 ]; then
    out "none"
  else
    out "Installing AUR dependencies for '${PKG}'..."
    out "Packages: ${DEP_PKGS}"
    increase
      sudo pacman -U --noconfirm ${DEP_PKGS} 2>&1 | output /packages/${PKG}.deps_install.log
    decrease
    out "Installed $count AUR dependencies for '${PKG}'"
  fi
  decrease
}

buildpkg () {
  local PKG="$1"
  local PARENT="$2"
  local PKG_TAR
  local SRC_TAR
  RET_PKGS=""

  if [ -n "${PARENT}" ]; then
    out "Building package ${PKG} as dependency for ${PARENT}..."
  else
    out "Building package ${PKG}..."
  fi
  cd "/builds/${PKG}"

  increase
    makepkg -Ccd 2>&1 | output /packages/${PKG}.log
  decrease

  PKG_TAR="$(sudo find "/builds/${PKG}" -name '*.pkg.tar*' | sort -Vru | head -1)"
  SRC_TAR="$(sudo find "/builds/${PKG}" -name '*.src.tar*' | sort -Vru | head -1)"

  RET_PKGS="${PKG_TAR} ${SRC_TAR}"
  BUILT_PACKAGES="${BUILT_PACKAGES} ${PKG_TAR} ${SRC_TAR}"

  if [ -z "${RET_PKGS}" ]; then
    err "Package '${PKG}' does not have pkg/src file"
    ERRORS=$(( $ERRORS + 1 ))
    MISSING="${MISSING} ${PKG}"
  else
    out "Package '${PKG}' has been built"
    BUILT="${BUILT} ${PKG}"
  fi

  cd /builds
}

out "Collecting dependencies..."
increase
for PKG in ${@}; do
  collectdeps "${PKG}"
done
decrease

## Trim whitespace + merge with forced
UPSTREAM="$(echo "${FORCED_PKGS} ${UPSTREAM}" | tr " " "\n" | sort -Vu | xargs)"

out "Installing upstream dependencies..."
increase
  out "Packages: ${UPSTREAM}"
  sudo pacman -S --noconfirm --needed ${UPSTREAM} 2>&1 | output
decrease

## Trim whitespace
GPG="$(echo "${GPG}" | xargs)"

if [ ${#GPG} -eq 0 ]; then
  out "Skipping GPG key import (no keys required)..."
else
  out "Importing GPG keys..."
  increase
    gpg --recv-keys ${GPG} 2>&1 | output
  decrease
fi

out "Installing AUR dependencies..."
increase
for PKG in ${AUR}; do
  installmakedeps "${PKG}"
done
decrease

out "Building AUR packages..."
increase
for PKG in ${AUR}; do
  buildpkg "${PKG}"
  BUILT_PACKAGES="${RET_PKGS}"
done
decrease

BUILT_PACKAGES="$(echo "${BUILT_PACKAGES}" | tr " " "\n" | sort -Vu | xargs)"

out "Built packages:" 2>&1 | output
increase
  echo "${BUILT_PACKAGES}" | output
decrease

out "Copying to target..." 2>&1 | output
increase
  sudo cp -v ${BUILT_PACKAGES} /packages/ 2>&1 | output
#  cd /packages
#  sudo repo-add -n -p repo.db.tar.gz ${BUILT_PACKAGES} 2>&1 | output
decrease

sudo chown -R 65534:65534 /packages 2>&1 | output
sudo chmod -R 777 /packages 2>&1 | output

## Trim whitespace
UPSTREAM="$(echo "${UPSTREAM}" | tr " " "\n" | sort -Vu | xargs)"
MISSING="$(echo "${MISSING}" | tr " " "\n" | sort -Vu | xargs)"
BUILT="$(echo "${BUILT}" | tr " " "\n" | sort -Vu | xargs)"

out "-----------------"
out "Summay:"
out "Input: $@"
out -e "Upstream dependencies\t: ${UPSTREAM}"
out -e "Built AUR packages\t: ${BUILT}"
out -e "Failed AUR packages\t: ${MISSING}"

exit $ERRORS
