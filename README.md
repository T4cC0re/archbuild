# archbuild

###### Builds AUR packages (and dependencies) in an ephermeral Docker container

## Usage

1. Clone repo and run
```
./build.sh <package1> <package2...> <...>
```
2. your package(s) will be in `./packages`
3. Done!

## License

MIT
